# The database

When Tezos DigiSign is launched, the database is initialized by the daemon using migrations. To modify the database,
we can create new migrations and restart the daemon. By doing so, all the migrations that were added and never applied
by the daemon will be executed when it restarts.

# Update the database with migrations

To update the database simply and rapidly, we can add migrations.
The migrations are executed in lib/model/src/main/kotlin/com/sword/signature/model/migration/MigrationHandler.kt and the
migration files are located in the directory lib/model/src/main/resources/migrations.

There are 4 possible actions on the database using migrations:

* Insert elements
* Update elements one by one
* Delete elements
* Update elements

To create a new migration, you will have to create a JSON file with the following name pattern:
versionNumber_nameOfMigration.json

For example, if you already have the migrations:

* 0.1_initDefaultAccounts.json
* 0.2_addSimpleAlgorithms.json
* 0.3_addAdminToken.json

If you want to create a migration to add a user, you will create the file : 0.4_addUser.json

Please, note that the migrations are not transactional. It means that if a migration fails before its end, the changes
already applied won't be canceled.

## Insert elements

This type of migration will add new elements in one of the database's collection.
The json file of this type of migration should look like this :

```json
{
  "collectionName": {
    "insert": [
      {
        "_id": "",
        "attr1": "",
        "attr2": "",
        "attr3": ""
      },
      {
        "_id": "",
        "attr1": "",
        "attr2": "",
        "attr3": ""
      }
    ]
  }
}
```

You will have to:

* Replace collectionName by the name of the collection you want to insert your elements in.
* Change "attr1", "attr2", "attr3" by the name of the attribute of your collection and then write the value that you
  want to set for these attributes and the "_id".

### Example

If you want to add the companies "Toto Corp" and "Tata Inc" to your database you will have to create the json :
**0.4_AddCompanies.json**

```json
{
  "tenants": {
    "insert": [
      {
        "_id": "6278cbba19413d0f03fd90aa",
        "name": "Toto Corp",
        "email": "toto.corp@mail.com",
        "tezosAddress": "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcja",
        "defaultGroupId": "62879d4c31a42273a825817e"
      },
      {
        "_id": "6278cbba19413d0f03fd90zz",
        "name": "Tata Inc",
        "email": "tata.inc@mail.com",
        "tezosAddress": "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjz",
        "defaultGroupId": "62879d4c31a42273a825817e"
      }
    ]
  }
}
```

In this example:

* We will apply the migration in the collection "tenants".
* The migration type is "insert".
* We will add two tenants with the displayed data.

## Update elements one by one

This type of migration will update one by one, elements of the database's collection.
The json file of this type of migration should look like this :

```json
{
  "collectionName": {
    "update": [
      {
        "_id": "",
        "attr1": "",
        "attr2": "",
        "attr3": ""
      },
      {
        "_id": "",
        "attr1": "",
        "attr2": "",
        "attr3": ""
      }
    ]
  }
}
```

You will have to:

* Replace collectionName by the name of the collection you want to update your elements in.
* Change "attr1", "attr2", "attr3" by the name of the attribute of your collection and then write the value that you
  want to set for these attributes.
* Please note that you will have to write all the attributes and the value you want to set to them even if the value
  doesn't change.
* Specify the ID of the elements you want to update.

### Example

If you want to update the email address of Toto Corp and the name of Toto Inc, you will have to create the JSON:
**0.5_UpdateCompanies.json**

```json
{
  "tenants": {
    "update": [
      {
        "_id": "6278cbba19413d0f03fd90aa",
        "name": "Toto Corp",
        "email": "toto.corporation@mail.com",
        "tezosAddress": "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcja",
        "defaultGroupId": "62879d4c31a42273a825817e"
      },
      {
        "_id": "6278cbba19413d0f03fd90zz",
        "name": "Tata Cie",
        "email": "tata.inc@mail.com",
        "tezosAddress": "tz1VSUr8wwNhLAzempoch5d6hLRiTh8Cjcjz",
        "defaultGroupId": "62879d4c31a42273a825817e"
      }
    ]
  }
}
```

In this example:

* We will apply the migration in the collection "tenants".
* The migration type is "update".
* We will update the elements which have the displayed IDs with the displayed data.

## Delete elements

This type of migrations will delete elements.
The json file of this type of migration should look like this :

```json
{
  "collectionName": {
    "delete": [
      {
        "_id": ""
      },
      {
        "_id": ""
      }
    ]
  }
}
```

You will have to :

* Replace collectionName by the name of the collection you want to delete your elements from.
* Specify the IDs of the elements you want to delete.

### Example

If you want to delete Toto Corp and Tata Cie from the companies. You will have to create the JSON:
**0.6_deleteCompanies.json**

```json
{
  "tenants": {
    "delete": [
      {
        "_id": "6278cbba19413d0f03fd90aa"
      },
      {
        "_id": "6278cbba19413d0f03fd90zz"
      }
    ]
  }
}
```

In this example:

* We will apply the migration in the collection "tenants".
* The migration type is "delete".
* We will delete the two tenants with the displayed IDs.

### Update multiple elements at the same time

This type of migration will update multiple elements at the same time. You can add new
attributes to a collection with a default value, and you can change all the values of one attribute.
You can also filter the elements of your collection with their attributes' value to select just the one you want to
update.

You can use 4 types of filter:

* equal: To select the elements with an attribute's value which matches a defined one.
* notEqual: To select the elements with an attribute's value which doesn't match a defined one.
* in: To select the elements which has its attribute's value that is contained in a defined list.
* notIn: To select the elements which has its attribute's value that isn't contained in a defined list.

**If you use the filters "equal" and "notEqual"**

The json file of this type of migration should look like this :

```json
{
  "collectionName": {
    "updateMany": {
      "criteria": {
        "filter": "",
        "type": "",
        "fieldName": "",
        "value": ""
      },
      "documentsToUpdate": [
        {
          "$addFields": {
            "attr1": "",
            "attr2": ""
          }
        },
        {
          "$set": {
            "attr1": "",
            "attr2": ""
          }
        }
      ]
    }
  }
}
```

You will have to :

* Replace collectionName by the name of the collection you want to update your elements in.
* Set the "filter" that you want to use for your criteria (equal or notEqual).
* Specify the attribute you want to base your criteria on (fieldName) and the requested value of this attribute.
* Specify the "type" of the attribute's value : "String", "Long", "Boolean"
* If you want to add new fields to your collection:
  * In the  "$addFields" section, replace attr1 by the name of the field you want to add and specify its default
    value.
  * Add as many attributes as you want.
  * Please note that the value will be set only for the elements who match the criteria.
* If you want to set a default value to an attribute:
  * In the "$set" section, replace attr1 by the name of the attribute you want to update and specify its new value.
  * Add as many attributes as you want.
  * Please note that the value will be set only for the elements who match the criteria.
* If you don't want to add new fields or to change the value of an attribute, please delete the useless section in the
  JSON file.
*

### Example

```json
{
  "accounts": {
    "updateMany": {
      "criteria": {
        "filter": "equal",
        "type": "String",
        "fieldName": "login",
        "value": "frodon"
      },
      "documentsToUpdate": [
        {
          "$addFields": {
            "test": "Hello"
          }
        },
        {
          "$set": {
            "company": "Toto Inc",
            "country": "Spain"
          }
        }
      ]
    }
  }
}
```

In this example:

* We will apply the migration in the collection "accounts".
* The migration type is "updateMany".
* The criteria filter is defined to "equal", the fieldName for the criteria to "login", the value to "frodon" and the
  value's type to "String". So only the accounts with frodon as their login will be updated.
* In the "$addFields" section, we specified that we want to add the column "test" to our collection and the default
  value will be "Hello" for those who match the criteria.
* In the "$set" section, we specified that we want to change the value for two columns:
  * We will set the "company" value to "Toto Inc"
  * We will set the "country" value to "Spain"
  * The changes will be applied only for the elements who match the criteria

**If you use the filters "in" and "notIn"**

The json file of this type of migration should look like this :

```json
{
  "collectionName": {
    "updateMany": {
      "criteria": {
        "filter": "",
        "type": "",
        "fieldName": "",
        "values": [
          {
            "value": ""
          },
          {
            "value": ""
          }
        ]
      },
      "documentsToUpdate": [
        {
          "$addFields": {
            "attr1": "",
            "attr2": ""
          }
        },
        {
          "$set": {
            "attr1": "",
            "attr2": ""
          }
        }
      ]
    }
  }
}
```

You will have to :

* Replace collectionName by the name of the collection you want to update your elements in.
* Set which "filter" will be used for your criteria (in or notIn).
* Specify the attribute you want to base your criteria on (fieldName).
* Specify the "type" of the values: "Boolean", "String" or "Long"
* For all the values that your fieldType must have (with "in") or not (with "notIn"), you will have to add the section
  {"value": ""} and specify one of the value.
* if you want to add new fields to your collection:
  * In the "$addFields" section, replace attr1 by the name of the field you want to add and specify its default
    value.
  * Add as many attributes as you want.
  * Please note that the value will be set only for the elements who match the criteria.
* If you want to set a default value to an attribute:
  * In the "$set" section, replace attr1 by the name of the attribute you want to update and specify its new value.
  * Add as many attributes as you want.
  * Please note that the value will be set only for the elements who match the criteria.
* If you don't want to add new fields or to change the value of an attribute, please delete the useless section in the
  JSON file.

### Example

```json
{
  "accounts": {
    "updateMany": {
      "criteria": {
        "filter": "notIn",
        "type": "String",
        "fieldName": "country",
        "value": [
          {
            "value": "Spain"
          },
          {
            "value": "France"
          }
        ]
      },
      "documentsToUpdate": [
        {
          "$set": {
            "country": "Other Country"
          }
        }
      ]
    }
  }
}
```

In this example:

* We will apply the migration in the collection "accounts".
* The migration type is "updateMany".
* The criteria filter is defined to "notIn", its fieldName to "country", the values for the criteria are "France" and
  "Spain" and the values' type is "String". So only the accounts with something different from France and Spain will be
  changed by this migration.
* Theres isn't any "$addFields" section, so we will not add any columns.
* In the "$set" section, we specified that we want to change the value for the column "country" to be set to "Other
  Country". Only the accounts who match the criteria will be updated.