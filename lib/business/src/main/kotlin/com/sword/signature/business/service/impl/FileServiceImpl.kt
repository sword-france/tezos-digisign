package com.sword.signature.business.service.impl

import com.sword.signature.business.exception.EntityNotFoundException
import com.sword.signature.business.exception.MissingRightException
import com.sword.signature.business.model.*
import com.sword.signature.business.model.mapper.toBusiness
import com.sword.signature.business.service.FileService
import com.sword.signature.business.service.JobService
import com.sword.signature.business.service.dummyAdminAccount
import com.sword.signature.common.criteria.FileCriteria
import com.sword.signature.common.criteria.JobCriteria
import com.sword.signature.common.criteria.TreeElementCriteria
import com.sword.signature.common.enums.JobStateType
import com.sword.signature.common.enums.TreeElementPosition
import com.sword.signature.model.entity.JobEntity
import com.sword.signature.model.entity.TreeElementEntity
import com.sword.signature.model.mapper.toPredicate
import com.sword.signature.model.repository.JobRepository
import com.sword.signature.model.repository.TreeElementRepository
import eu.coexya.tezos.connector.service.TezosReaderService
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactive.awaitFirst
import kotlinx.coroutines.reactive.awaitFirstOrNull
import kotlinx.coroutines.reactive.awaitLast
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class FileServiceImpl(
    private val treeElementRepository: TreeElementRepository,
    private val jobRepository: JobRepository,
    private val jobService: JobService,
    private val tezosReaderService: TezosReaderService,
    @Value("\${tezos.chain:#{null}}") private val tezosChain: String?,
    @Value("\${tezos.urls.api.storage:#{null}}") private val apiStorageUrl: String?,
    @Value("\${tezos.urls.api.transaction:#{null}}") private val apiTransactionUrl: String?,
    @Value("\${tezos.urls.web.provider:#{null}}") private val webProviderUrl: String?
) : FileService {
    override suspend fun getFileProof(requester: Account, fileId: String): Proof? {

        val leafElement = treeElementRepository.findById(fileId).awaitFirstOrNull() ?: return null
        var job = jobService.findByIdSignatureJob(dummyAdminAccount, leafElement.jobId, false)!!

        // Check right to perform operation.
        if (!requester.isAdmin && requester.id != job.userId) {
            throw MissingRightException(requester)
        }

        // Update job with transaction timestamp if required (legacy jobs).
        if (job.state >= JobStateType.VALIDATED && job.transactionHash != null && job.contractAddress != null && job.blockHash != null
            && job.rootHash != null
        ) {

            val storageValue =
                tezosReaderService.getValueFromContractStorage(job.contractAddress!!, job.rootHash!!)

            if (storageValue == null) {
                LOGGER.warn("There should be an entry in the storage for this signature.")
            } else if (job.timestamp == null || job.timestamp != storageValue.timestamp) {
                job =
                    jobService.patchJob(dummyAdminAccount, job.id, JobPatch(timestamp = storageValue.timestamp)) as Job
            }
        }

        LOGGER.debug("Retrieving proof for file {}.", leafElement.metadata?.fileName)
        // Retrieve all siblings first.
        val elements = mutableListOf<Pair<String?, TreeElementPosition>>()
        var nextParent = leafElement.parentId?.let { treeElementRepository.findById(it).awaitFirst() }
        var element = leafElement
        while (nextParent != null) {
            val nextSiblingElement = findSibling(element.id!!, element.parentId)
            LOGGER.debug("Element {} added in siblings list.", nextSiblingElement?.id)
            elements.add(
                if (nextSiblingElement == null) {
                    Pair(
                        null, if (element.position == TreeElementPosition.RIGHT) {
                            TreeElementPosition.LEFT
                        } else {
                            TreeElementPosition.RIGHT
                        }
                    )
                } else {
                    Pair(
                        nextSiblingElement.hash,
                        nextSiblingElement.position!! // Position is not nullable in a tree element.
                    )
                }
            )
            element = nextParent
            nextParent = element.parentId?.let { treeElementRepository.findById(it).awaitFirst() }
        }

        // Add url nodes
        val urlNodes = mutableListOf<URLNode>()
        // API Storage Url
        if (apiStorageUrl != null && job.contractAddress != null) {
            getContractStorageId(job.contractAddress!!).let {
                urlNodes.add(
                    URLNode.fromApiStorageUrl(
                        url = apiStorageUrl,
                        bigMapId = it.toString(),
                        rootHash = element.hash
                    )
                )
            }
        }
        // API Transaction Url
        if (apiTransactionUrl != null && job.transactionHash != null) {
            urlNodes.add(
                URLNode.fromApiTransactionUrl(
                    url = apiTransactionUrl,
                    transactionHash = job.transactionHash!!
                )
            )
        }
        // Web Provider Url
        webProviderUrl?.let {
            urlNodes.add(URLNode.fromWebProviderUrl(url = it))
        }

        return Proof(
            signatureDate = job.timestamp,
            filename = leafElement.metadata?.fileName,
            algorithm = job.algorithm,
            documentHash = leafElement.hash,
            rootHash = element.hash,
            hashes = elements,
            customFields = leafElement.metadata?.customFields,
            contractAddress = job.contractAddress,
            transactionHash = job.transactionHash,
            blockChain = tezosChain,
            blockHash = job.blockHash,
            signerAddress = job.signerAddress,
            urls = urlNodes
        )
    }


    suspend fun getJob(jobId: String, jobs: MutableMap<String, Job>): Job {
        val job: Job
        if (jobs.containsKey(jobId)) {
            job = jobs[jobId]!!
            LOGGER.debug("Job (id={}) was retrieved from cache.", jobId)
        } else {
            job = jobRepository.findById(jobId).awaitFirstOrNull()?.toBusiness()
                ?: throw EntityNotFoundException("Job", jobId)
            LOGGER.debug("Job (id={}) was retrieved from database and put into cache.", jobId)
            jobs[jobId] = job
        }
        return job
    }

    override suspend fun getFiles(
        requester: Account,
        filter: FileFilter?,
        pageable: Pageable
    ): Flow<TreeElement.LeafTreeElement> {

        // Check right to perform operation.
        if (!requester.isAdmin && requester.id != filter?.accountId) {
            throw MissingRightException(requester)
        }

        LOGGER.debug("Find the list of documents.")

        val fileCriteria = buildFileCriteria(filter)
        val files = treeElementRepository.findAll(fileCriteria.toPredicate(), pageable.sort)
        val jobs = hashMapOf<String, Job>()

        return files.asFlow().paginate(pageable)
            .map { it.toBusiness(getJob(it.jobId, jobs)) as TreeElement.LeafTreeElement }
    }


    override suspend fun countFiles(
        requester: Account,
        filter: FileFilter?
    ): Long {
        // Check right to perform operation.
        if (!requester.isAdmin && requester.id != filter?.accountId) {
            throw MissingRightException(requester)
        }

        val fileCriteria = buildFileCriteria(filter)
        return treeElementRepository.count(fileCriteria.toPredicate()).awaitLast()
    }

    private suspend fun buildFileCriteria(filter: FileFilter?): FileCriteria {
        if (filter != null) {
            val jobCriteria: JobCriteria? =
                if (filter.accountId != null || filter.dateStart != null || filter.dateEnd != null)
                    JobCriteria(
                        accountId = filter.accountId,
                        dateStart = filter.dateStart,
                        dateEnd = filter.dateEnd
                    ) else null

            val allowedJobsIds: List<String>? =
                jobCriteria?.let {
                    jobRepository.findAll(jobCriteria.toPredicate<JobEntity>()).asFlow().map { it.id!! }.toList()
                }

            return FileCriteria(
                id = filter.id,
                name = filter.name,
                hash = filter.hash,
                jobId = filter.jobId,
                jobIds = allowedJobsIds
            )
        } else {
            return FileCriteria()
        }
    }

    private suspend fun findSibling(id: String, parentId: String?): TreeElementEntity? {
        if (parentId == null) {
            return null
        }
        val treeElementPredicate = TreeElementCriteria(notId = id, parentId = parentId).toPredicate()
        return treeElementRepository.findOne(
            treeElementPredicate
        ).awaitFirstOrNull()
    }

    // Local index of contracts
    private val contractsStorageIds = mutableMapOf<String, String?>()

    private suspend fun getContractStorageId(contractAddress: String): String? {
        return if (contractsStorageIds.containsKey(contractAddress) && contractsStorageIds[contractAddress] != null) {
            contractsStorageIds[contractAddress]
        } else {
            tezosReaderService.getContractStorageId(contractAddress)
        }
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(FileServiceImpl::class.java)
    }
}
