package com.sword.signature.rest.configuration

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.config.CorsRegistry
import org.springframework.web.reactive.config.WebFluxConfigurer

@Configuration
class DeveloperWebFluxConfigurer(
    @Value("\${security.allowedOrigin}") val allowedOrigin: String,
    @Value("\${security.openCors}") val openCors: String
): WebFluxConfigurer {

    override fun addCorsMappings(corsRegistry: CorsRegistry) {
        val mapping = corsRegistry.addMapping("/**")
            .allowCredentials( true)
            .allowedHeaders("*")
            .allowedMethods("*")

        if(openCors.toBoolean()){
            mapping.allowedOriginPatterns("*")
        }else{
            mapping.allowedOrigins(allowedOrigin)
        }
    }
}
