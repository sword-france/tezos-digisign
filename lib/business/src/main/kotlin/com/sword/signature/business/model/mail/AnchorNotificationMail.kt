package com.sword.signature.business.model.mail

import com.sword.signature.business.model.Account
import com.sword.signature.business.model.Job
import org.thymeleaf.context.Context
import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter

class AnchorNotificationMail(
    recipient: Account,
    private val jobId: String,
    private val origin: String,
    private val flowName: String,
    private val docsNumber: Int,
    private val injectedDate: OffsetDateTime?
) : TransactionalMail(
    recipient = recipient,
    templateName = "anchor_notification_mail",
    subject = "$subjectTag Your job '${flowName}' (${docsNumber} ${if (docsNumber > 1) "files" else "file"}) has been anchored the " +
            DateTimeFormatter.ofPattern("dd/MM/yyyy' at 'HH:mm").format(injectedDate)
) {
    override fun getContext() = Context().apply {
        setVariable("name", recipient.fullName)
        setVariable("link", "${origin}/#/jobs/${jobId}")
        setVariable("flowName", flowName)
        setVariable("docsNumber", docsNumber)
        setVariable("injectedDate", DateTimeFormatter.ofPattern("dd/MM/yyyy' 'HH:mm").format(injectedDate))
    }
}
