package com.sword.signature.business.service.impl

import com.sword.signature.business.exception.EntityNotFoundException
import com.sword.signature.business.exception.ServiceException
import com.sword.signature.business.model.*
import com.sword.signature.business.model.mapper.toBusiness
import com.sword.signature.business.service.JobService
import com.sword.signature.common.criteria.JobCriteria
import com.sword.signature.common.criteria.TreeElementCriteria
import com.sword.signature.common.enums.JobStateType
import com.sword.signature.common.enums.TreeElementPosition
import com.sword.signature.common.enums.TreeElementType
import com.sword.signature.model.entity.*
import com.sword.signature.model.mapper.toPredicate
import com.sword.signature.model.repository.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactive.awaitFirstOrNull
import kotlinx.coroutines.reactive.awaitSingle
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import java.time.OffsetDateTime

@Service
class JobServiceImpl(
    private val tezosOperationJobRepository: TezosOperationJobRepository,
    private val signatureJobRepository: JobRepository,
    private val transferJobRepository: TransferJobRepository,
    private val revealJobRepository: RevealJobRepository,
    private val treeElementRepository: TreeElementRepository

) : JobService {

    override fun findAll(
        requester: Account,
        criteria: JobCriteria?,
        pageable: Pageable
    ): Flow<TezosOperationJob> {
       return findAllGeneric<TezosOperationJobEntity>(requester, criteria, pageable).asFlow()
            .paginate(pageable)
            .map { it.toBusiness() }
    }

    override fun findAllSignatureJob(
        requester: Account,
        criteria: JobCriteria?,
        pageable: Pageable
    ): Flow<Job> {
        return findAllGeneric<JobEntity>(requester, criteria, pageable).asFlow()
            .paginate(pageable)
            .map { it.toBusiness() }
    }

    override fun findAllTransferJob(
        requester: Account,
        criteria: JobCriteria?,
        pageable: Pageable
    ): Flow<TransferJob> {
        return findAllGeneric<TransferJobEntity>(requester, criteria, pageable).asFlow()
            .paginate(pageable)
            .map { it.toBusiness() }
    }

    override fun findAllRevealJob(
        requester: Account,
        criteria: JobCriteria?,
        pageable: Pageable
    ): Flow<RevealJob> {
        return findAllGeneric<RevealJobEntity>(requester, criteria, pageable).asFlow()
            .paginate(pageable)
            .map { it.toBusiness() }
    }

    private inline fun <reified T : TezosOperationJobEntity> findAllGeneric(
        requester: Account,
        criteria: JobCriteria?,
        pageable: Pageable
    ): Flux<out T> {
        checkAccess(requester, criteria)

        val repository = when (T::class) {
            JobEntity::class -> signatureJobRepository
            TransferJobEntity::class -> transferJobRepository
            RevealJobEntity::class -> revealJobRepository
            else -> tezosOperationJobRepository
        }

        return if (criteria != null) {
            repository.findAll(criteria.toPredicate<T>(), pageable.sort)
        } else {
            repository.findAll(pageable.sort)
        } as Flux<out T>
    }

    override fun countAllSignatureJob(requester: Account, criteria: JobCriteria?): Long {
        return countAllGeneric<JobEntity>(requester, criteria)
    }

    override fun countAllTransferJob(requester: Account, criteria: JobCriteria?): Long {
        return countAllGeneric<TransferJobEntity>(requester, criteria)
    }

    override fun countAllRevealJob(requester: Account, criteria: JobCriteria?): Long {
        return countAllGeneric<RevealJobEntity>(requester, criteria)
    }

    private inline fun <reified T : TezosOperationJobEntity> countAllGeneric(
        requester: Account,
        criteria: JobCriteria?
    ): Long {
        checkAccess(requester, criteria)
        val repository = when (T::class) {
            JobEntity::class -> signatureJobRepository
            TransferJobEntity::class -> transferJobRepository
            RevealJobEntity::class -> revealJobRepository
            else -> throw IllegalStateException("Incorrect type provided.")
        }

        return if (criteria != null) {
            repository.count(criteria.toPredicate<T>()).block()!!
        } else {
            repository.count().block()!!
        }
    }

    private fun checkAccess(requester: Account, criteria: JobCriteria?) {
        if (!requester.isAdmin && requester.id != criteria?.accountId) {
            throw IllegalAccessException("user ${requester.login} does not have role/permission to access jobs for accountId=[${criteria?.accountId}]")
        }
    }

    override suspend fun findByIdJob(requester: Account, jobId: String): TezosOperationJob? {
        val job = tezosOperationJobRepository.findById(jobId).awaitFirstOrNull() ?: return null
        LOGGER.debug("id : {}, found {}", job.id, job)
        checkAccessJob(requester, job)

        if (job is JobEntity) {
            return job.toBusiness(retrieveRootHash(jobId))
        }

        return job.toBusiness()
    }

    override suspend fun findByIdSignatureJob(requester: Account, jobId: String, withLeaves: Boolean): Job? {
        val job = signatureJobRepository.findById(jobId).awaitFirstOrNull() ?: return null
        LOGGER.debug("id : {}, found {}", job.id, job)
        checkAccessJob(requester, job)

        val leaves =
            if (withLeaves) {
                retrieveLeaves(job)
            } else {
                null
            }

        return job.toBusiness(retrieveRootHash(jobId), leaves)
    }

    override suspend fun findByIdTransferJob(requester: Account, jobId: String): TransferJob? {
        val job = transferJobRepository.findById(jobId).awaitFirstOrNull() ?: return null
        LOGGER.debug("id : {}, found {}", job.id, job)
        checkAccessJob(requester, job)
        return job.toBusiness()
    }

    override suspend fun findByIdRevealJob(requester: Account, jobId: String): RevealJob? {
        val job = revealJobRepository.findById(jobId).awaitFirstOrNull() ?: return null
        LOGGER.debug("id : {}, found {}", job.id, job)
        checkAccessJob(requester, job)
        return job.toBusiness()
    }

    private suspend fun retrieveLeaves(job: JobEntity): List<TreeElement.LeafTreeElement> {
        // Retrieve the leaves if needed.
        val treeElementPredicate = TreeElementCriteria(jobId = job.id, type = TreeElementType.LEAF).toPredicate()
        val leaves =
            treeElementRepository.findAll(treeElementPredicate).asFlow()
                .map { it.toBusiness(job.toBusiness()) as TreeElement.LeafTreeElement }.toList()
        LOGGER.debug("Job {}: leaves retrieved {}.", job.id, leaves)
        return leaves
    }

    private suspend fun retrieveRootHash(jobId: String): String? {
        // Retrieve the merkle tree root hash.
        val rootElementPredicate =
            QTreeElementEntity.treeElementEntity.parentId.isNull.and(QTreeElementEntity.treeElementEntity.jobId.eq(jobId))
        return treeElementRepository.findOne(rootElementPredicate).awaitFirstOrNull()?.hash
    }

    override suspend fun getMerkelTree(requester: Account, jobId: String): MerkelTree? {
        val job = signatureJobRepository.findById(jobId).awaitFirstOrNull() ?: return null
        LOGGER.debug("id : {}, found {}", job.id, job)
        checkAccessJob(requester, job)

        // Retrieve the nodes
        val treeElementPredicate = TreeElementCriteria(jobId = jobId).toPredicate()
        val elements = treeElementRepository.findAll(treeElementPredicate).asFlow().toList()
            .associateBy { it.id }
        val nodes = elements.mapValues { MerkelTree.Node(hash = it.value.hash) }
        var rootNode: MerkelTree.Node? = null
        elements.forEach {
            if (it.value.parentId != null) {
                val parentNode = nodes.getValue(it.value.parentId)
                val node = nodes.getValue(it.key)
                if (it.value.position == TreeElementPosition.LEFT) {
                    parentNode.left = node
                } else {
                    parentNode.right = node
                }
            } else {
                rootNode = nodes.getValue(it.key)
            }
        }
        if (rootNode == null) {
            throw ServiceException("error while getting the tree for JobId $jobId")
        }

        LOGGER.debug("Job {}: tree retrieved {}.", jobId, rootNode)

        return MerkelTree(
            algorithm = job.algorithm,
            root = rootNode!!
        )
    }

    override suspend fun patchSignatureJob(requester: Account, jobId: String, patch: JobPatch): Job {
        return patchGeneric(requester, jobId, patch)
    }

    override suspend fun patchTransferJob(requester: Account, jobId: String, patch: JobPatch): TransferJob {
        return patchGeneric(requester, jobId, patch)
    }

    override suspend fun patchRevealJob(requester: Account, jobId: String, patch: JobPatch): RevealJob {
        return patchGeneric(requester, jobId, patch)
    }

    override suspend fun patchJob(requester: Account, jobId: String, patch: JobPatch): TezosOperationJob {
        return patchGeneric(requester, jobId, patch)
    }

    private suspend inline fun <reified T : TezosOperationJob> patchGeneric(
        requester: Account,
        jobId: String,
        patch: JobPatch
    ): T {

        val repository = when (T::class) {
            Job::class -> signatureJobRepository
            TransferJob::class -> transferJobRepository
            RevealJob::class -> revealJobRepository
            else -> tezosOperationJobRepository
        }

        val job = repository.findById(jobId).awaitFirstOrNull() ?: throw EntityNotFoundException("job", jobId)
        LOGGER.debug("id : {}, found {}", jobId, job)
        checkAccessJob(requester, job)

        val updatedJob =
            when (job) {
                is JobEntity -> {
                    val toPatch = job.copy(
                        numberOfTry = patch.numberOfTry ?: job.numberOfTry,
                        transactionHash = patch.transactionHash ?: job.transactionHash,
                        timestamp = patch.timestamp ?: job.timestamp,
                        blockHash = patch.blockHash ?: job.blockHash,
                        blockDepth = patch.blockDepth ?: job.blockDepth,
                        state = patch.state ?: job.state,
                        callBackStatus = patch.callBackStatus ?: job.callBackStatus,
                        injectedDate = if (patch.state == JobStateType.INJECTED) {
                            OffsetDateTime.now()
                        } else {
                            job.injectedDate
                        },
                        validatedDate = if (patch.state == JobStateType.VALIDATED) {
                            OffsetDateTime.now()
                        } else {
                            job.validatedDate
                        },
                        stateDate = if (patch.state != null) {
                            OffsetDateTime.now()
                        } else {
                            job.stateDate
                        },
                        contractAddress = patch.contractAddress ?: job.contractAddress,
                        signerAddress = patch.signerAddress ?: job.signerAddress
                    )
                    signatureJobRepository.save(toPatch).awaitSingle().toBusiness()
                }
                is TransferJobEntity -> {
                    val toPatch = job.copy(
                        numberOfTry = patch.numberOfTry ?: job.numberOfTry,
                        transactionHash = patch.transactionHash ?: job.transactionHash,
                        timestamp = patch.timestamp ?: job.timestamp,
                        blockHash = patch.blockHash ?: job.blockHash,
                        blockDepth = patch.blockDepth ?: job.blockDepth,
                        state = patch.state ?: job.state,
                        injectedDate = if (patch.state == JobStateType.INJECTED) {
                            OffsetDateTime.now()
                        } else {
                            job.injectedDate
                        },
                        validatedDate = if (patch.state == JobStateType.VALIDATED) {
                            OffsetDateTime.now()
                        } else {
                            job.validatedDate
                        },
                        stateDate = if (patch.state != null) {
                            OffsetDateTime.now()
                        } else {
                            job.stateDate
                        },
                        signerAddress = patch.signerAddress ?: job.signerAddress
                    )
                    transferJobRepository.save(toPatch).awaitSingle().toBusiness()
                }
                is RevealJobEntity -> {
                    val toPatch = job.copy(
                        numberOfTry = patch.numberOfTry ?: job.numberOfTry,
                        transactionHash = patch.transactionHash ?: job.transactionHash,
                        timestamp = patch.timestamp ?: job.timestamp,
                        blockHash = patch.blockHash ?: job.blockHash,
                        blockDepth = patch.blockDepth ?: job.blockDepth,
                        state = patch.state ?: job.state,
                        injectedDate = if (patch.state == JobStateType.INJECTED) {
                            OffsetDateTime.now()
                        } else {
                            job.injectedDate
                        },
                        validatedDate = if (patch.state == JobStateType.VALIDATED) {
                            OffsetDateTime.now()
                        } else {
                            job.validatedDate
                        },
                        stateDate = if (patch.state != null) {
                            OffsetDateTime.now()
                        } else {
                            job.stateDate
                        },
                        signerAddress = patch.signerAddress ?: job.signerAddress
                    )
                    revealJobRepository.save(toPatch).awaitSingle().toBusiness()
                }
            }

        LOGGER.trace("job with id ({}) updated.", jobId)
        return updatedJob as T
    }

    private fun checkAccessJob(requester: Account, job: TezosOperationJobEntity) {
        if (!requester.isAdmin && requester.id != job.userId) {
            throw IllegalAccessException("user ${requester.login} does not have role/permission to get job: ${job.id}")
        }
    }

    companion object {
        val LOGGER: Logger = LoggerFactory.getLogger(JobServiceImpl::class.java)
    }
}
