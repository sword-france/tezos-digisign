package com.sword.signature.api.sign

data class RetrySignResponse(
    val valid: Boolean,
)
